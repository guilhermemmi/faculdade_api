const callback = require('./callback')
// Busca no banco de dados através do _id do objeto.
module.exports = (organism, query, res) => {
  organism.find({cpf: query.cpf}, {senha: query.senha}, (err,data) => {
    callback(err, res, data)
    if (err) {
      res.send(false)
    } else {
      res.send(true)
    }
  })
}
