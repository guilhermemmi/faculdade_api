const callback = require('./callback')
// Atualiza um objeto no banco de dados a partir do seu id.
module.exports = (organism, query, res, req) => {
  organism.update({_id: query.id}, req.body, (err,data) => {
    callback(err, res, data)
  })
}
