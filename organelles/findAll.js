const callback = require('./callback')
// Executa a query e retorna um array com os objetos que forem encontrados.
module.exports = (organism, query, res) => {
  organism.find({}, (err,data) => {
    callback(err, res, data)
  })
}
