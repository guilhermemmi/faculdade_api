const callback = require('./callback')

module.exports = (organism, query, res, req) => {
  organism
  .findOneAndUpdate(
      {_id: query._id},
      // essaa parte eu passo {consultas: _id}
      {$addToSet: {historico: query.historico}},
      (err, data) => {
         callback(err, res, data)
      }
  )
}
