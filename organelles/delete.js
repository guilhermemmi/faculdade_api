const callback = require('./callback')
// Remove do banco de dados um objeto com base no id passado.
module.exports = (organism, query, res) => {
  organism.remove({_id: query.id}, (err, data) => {
    callback(err, res, data)
  })
}
