const callback = require('./callback')
// Busca no banco de dados através do cpf do objeto.
module.exports = (organism, query, res) => {
  console.log("Searching for an User by its CPF.")
  console.log(query.cpf)
  organism.find({cpf: query.cpf}, (err,data) => {
    callback(err, res, data)
  })
}
