const callback = require('./callback')
// Busca no banco de dados através do _id do objeto.
module.exports = (organism, query, res) => {
  organism.find({_id: query._id}, (err,data) => {
    callback(err, res, data)
  })
}
