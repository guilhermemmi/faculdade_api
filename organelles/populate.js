const callback = require('./callback')
// isso aki eh do moongose que eh um ODM ele faz um basicamente um inner join
module.exports = (organism, query, res, req)=> {
  organism
  .findOne({email: query.email})
  //aki eu passo a coluna que vai ser populada
  .populate(query.populate)
  .exec ((err, data) => {
    callback(err, res, data)
  })
}
