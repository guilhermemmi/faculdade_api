require('../config.js')
var mongoose = require('mongoose')

module.exports = (query, collection, res, req) => {
  const organism =
  require('./../organism/organism')
  (require('./../molecule/molecule-' + query.molecule), collection)

  require('./../organelles/' + query.type)(organism, query, res, req)
}
