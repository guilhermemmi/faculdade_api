'use strict'

// Schema do banco
const mongoose = require('mongoose'), Schema = mongoose.Schema

const _molecule = {
  nome : {type: String},
  matricula : {type: String},
  curso : {type: String},
  cpf : require('./../atoms/atom-cpf'),
  email : {type : String},
  senha : {type: String},
  periodo_atual : {type : String},
  historico :[
    {
      periodo : {type : String},
      nota : {type : String},
      status : {type : String},
      disciplinas : [
        {
          _id : {type : String},
          nome : {type: String},
          horario : {type : String},
          dificuldade : {type : String},
          nome_professor : {type : String}
        }]
  }]
}
const molecule = new mongoose.Schema(_molecule);
module.exports = molecule
