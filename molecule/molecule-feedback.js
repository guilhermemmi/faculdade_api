'use strict'

// Schema do banco
const mongoose = require('mongoose'), Schema = mongoose.Schema

const _molecule = {
  id : {type: Number},
  assunto : {type : String},
  mensagem : {type : String},
  email_estudante : {type : String}
}
const molecule = new mongoose.Schema(_molecule);
module.exports = molecule
