const mongoose = require('mongoose');

// Nome do banco
const uriDB = 'mongodb://user_global:globalpass@ds139791.mlab.com:39791/heroku_7v8n4f75';

// Arquivo de conexão
mongoose.connect(uriDB);
mongoose.connection.on('connected', function(){
console.log("Mongo default connection connected to " + uriDB);
});
 mongoose.connection.on('error', function(err){
  console.log("Mongo default connection error" + err);
  });
  mongoose.connection.on('disconnected', function(){
   console.log("Mongo default connection disconnected");
   });
   mongoose.connection.on('open', function(){
    console.log("Mongo default connection open");
    });
    process.on('SIGINT',function(){
        mongoose.connection.close(function(){
          console.log("The connection is closed");
          process.exit(0);
        });
     });
