const atom = {
  type: String,
  validate: require('./../quarks/isName'),
  required: true
}
module.exports = atom
