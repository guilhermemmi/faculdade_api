'use strict'
const express = require('express');
const app = express();
const bodyParser = require('body-parser')
const estudante = 'estudante'
const disciplina = 'disciplina'
const feedback = 'feedback'
app.use(bodyParser.json());
var urlencodedParser = bodyParser.urlencoded({ extended: true })
app.use(function (req, res, next) {
    res.setHeader('Access-Control-Allow-Origin', '*');
    res.setHeader('Access-Control-Allow-Methods', 'GET, POST, OPTIONS, PUT, PATCH, DELETE');
    res.setHeader('Access-Control-Allow-Headers', 'X-Requested-With,content-type');
    res.setHeader('Access-Control-Allow-Credentials', true);
    next();
});

app.get ('/estudante', urlencodedParser, (req, res) => {
  require('./DNA/dna')(req.query, estudante, res, req)
})

app.get ('/disciplina', urlencodedParser, (req, res) => {
  require('./DNA/dna')(req.query, disciplina, res, req)
})

app.get ('/feedback', urlencodedParser, (req, res) => {
  require('./DNA/dna')(req.query, feedback, res, req)
})

// respond with "hello world" when a GET request is made to the homepage
app.get('/', (req, res) => {
  res.send('Página principal para API do Desafio Z');
});

app.set('port', (process.env.PORT || 5000))
app.listen(app.get('port'), (req, res) => {
  console.log("API Faculdade_Z rodando em: http://%s:%s", app.get('port'))
})
